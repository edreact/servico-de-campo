import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Relatório</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
      


        <div className={styles.grid}>
          <a className={styles.card}>
          <h2>Relatório de serviço de campo</h3>
            <h3>NOMEMBRO DE 2021</h3>

            <form name="contact" method="POST" data-netlify="true" className={styles.card}>
            <input type="hidden" name="form-name" value="contact">
          <p>
            <label htmlfor="name">Name</label>
            <input type="text" id="name" name="name" />
          </p>
          <p>
            <label htmlfor="email">Email</label>
            <input type="text" id="email" name="email" />
          </p>
          <p>
            <label htmlfor="message">Message</label>
            <textarea id="message" name="message"></textarea>
          </p>
          <p>
            <button type="submit">Send</button>
          </p>
        </form>

          </a>

          

        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
